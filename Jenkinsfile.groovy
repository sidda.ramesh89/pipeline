pipeline {
    agent any
    stages {
        stage("build") {
          echo 'building application...'
          sh 'mvn build'
        }
    }
     stages {
        stage("test") {
          echo 'testing application...'
          sh 'mvn test'
        }
    }
     stages {
        stage("deploy") {
          echo 'deplyoing application...'
          sh 'mvn deploy'
        }
    }

